class AddIdentifierTokenToPosts < ActiveRecord::Migration[5.2]
  def change
    enable_extension 'pgcrypto'
    add_column :posts, :identifier_token, :uuid, default: 'gen_random_uuid()', null: false
    add_index :posts, :identifier_token, unique: true
  end

  def down
    disable_extension 'pgcrypto'
    remove_column :posts, :identifier_token
  end
end
